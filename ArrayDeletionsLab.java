public class ArrayDeletionsLab {
    private static void deleteElementByIndex(int[] arr, int index){
        int[] arrayAfterDeleteByIndex = {0,0,0,0};
        addElementNewArray(arr,arrayAfterDeleteByIndex);
        for(int i=0;i<arr.length;i++){
            if (i==index){
                for(int j =i;j<arrayAfterDeleteByIndex.length;j++){
                    arrayAfterDeleteByIndex[j]=arr[j+1];
                }
            }
        }
        printArray(arrayAfterDeleteByIndex);
    }

    private static void deleteElementByValue(int[] arr, int value){
        int[] arrayAfterDeleteByValue = {0,0,0,0};
        addElementNewArray(arr, arrayAfterDeleteByValue);
        for(int i=0;i<arr.length;i++){
            if (arr[i]==value){
                for(int j =i;j<arrayAfterDeleteByValue.length;j++){
                    arrayAfterDeleteByValue[j]=arr[j+1];
                }
            }
        }
        printArray(arrayAfterDeleteByValue);
    }


    //สร้างฟังก์ชั่นที่ใช้ในการแทนค่าใน array หนึ่่งไป array หนึ่ง
    private static void addElementNewArray(int[] arr,int[] newArray){
        for(int i = 0;i<newArray.length;i++){
            newArray[i]=arr[i];
        }
    }

    private static void printArray(int[] arr){
        for(int i=0;i<arr.length;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5} ;
        int index = 2;
        int value = 4;
        
        
        

        System.out.print("Array after deleting element at index 2:");
        deleteElementByIndex(arr, index);
        System.out.print("Array after deleting element at value 4:");
        deleteElementByValue(arr, value);
    }
}
